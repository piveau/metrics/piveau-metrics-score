package io.piveau

import io.piveau.pipe.connector.PipeConnector
import io.piveau.metrics.MetricsScoreVerticle
import io.vertx.core.DeploymentOptions
import io.vertx.core.Launcher
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait

class MainVerticle : CoroutineVerticle() {

    override suspend fun start() {
        vertx.deployVerticle(MetricsScoreVerticle::class.java, DeploymentOptions()).coAwait()
        PipeConnector.create(vertx).coAwait().publishTo(MetricsScoreVerticle.ADDRESS)
    }

}

fun main(args: Array<String>) {
    Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
}
